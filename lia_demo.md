# Testing the Interactive Widget

The widget should appear below this text.

Here's the version hosted on the repository:

<iframe width="960" height="600"></iframe>

<script sandbox="allow-forms allow-modals" allow="" referrerpolicy="no-referrer">

const iframeElem = document.querySelector("iframe");

iframeElem.credentialless = true;
iframeElem.title = "Fair Prinzipien";
iframeElem.src = "https://jonathan.a.hartman1.pages.rwth-aachen.de/liascript-html-frame-demo/demo1";

</script>

How did that go?
