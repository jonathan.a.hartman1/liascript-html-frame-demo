# LiaScript HTML Frame Demo

From a conversation with Ceren after the workshop - she would like include an HTML5 widget in a
LiaScript document, but would like to include the code that generates the widget in the same
repository as the LiaScript document.

References:
- https://github.com/ceryild/liascript-workshop-march20
